// Login button for desktop
var x = document.getElementsByClassName("top-bar-style-1")[0];
var tag = document.createElement("div");
tag.setAttribute("id","gigya-login");
tag.innerHTML = "";
x.appendChild(tag);

var logout = document.createElement("div");
var logout_mobile = logout.cloneNode(true);
logout.setAttribute("id","gigya_controls_logout");
logout_mobile.setAttribute("id","gigya_controls_logout_mobile");
logout.innerHTML = "";
x.appendChild(logout);

// Login button for mobile
var m = document.getElementById("td-outer-wrap");
var before = document.getElementsByClassName("td-main-content-wrap")[0];
var mobile_header = document.createElement("div");
var mobile_button = document.createElement("div");
var cats = document.getElementsByClassName("td-category-header")[0];
mobile_header.setAttribute("id","mobile-login-header");
mobile_button.setAttribute("id","mobile-login-button");
mobile_button.innerHTML = "Iniciar Sesión";
mobile_header.appendChild(logout_mobile);
mobile_header.appendChild(mobile_button);
if(cats != null)
  m.insertBefore(mobile_header, cats);
else
  m.insertBefore(mobile_header, before);

var GIGYA_API = '3_-VnBr9fbvOvK9nEAthkxbSbYF2BSKd8o9nVgT8u3CXAYybNuAVI2rKI5lmU3mS1v';
var GIGYA_LANG = 'es';
var GIGYA_COUNTRY = 'en';
var GIGYA_LOGIN_SCREEN_SET = 'Default-RegistrationLogin';
var GIGYA_OTHERCATEGORY_LOGIN_SCREEN_SET = 'ScreenCatRestantes-RegistrationLogin';
var GIGYA_HOMECATEGORY_LOGIN_SCREEN_SET = 'ScreenHome-RegistrationLogin';
      
var GIGYA_DINAMICA_SCREEN = 'gigya-login-screen';
var GIGYA_OTHERCATEGORY_SCREEN = 'gigya-login-screen';
var GIGYA_HOMECATEGORY_SCREEN = 'gigya-login-screen';

var GIGYA_NEWS_DINAMICA_SCREEN = 'NewsletterRegistration';
var GIGYA_NEWS_OTHERCATEGORY_SCREEN = 'gigya-register-screen';
var GIGYA_NEWS_HOMECATEGORY_SCREEN = 'gigya-register-screen';

var GIGYA_PROFILE_SCREEN_SET = 'Default-ProfileUpdate';
var GIGYA_OTHERCATEGORY_PROFILE_SCREEN_SET = 'ScreenCatRestantes-ProfileUpdate';
var GIGYA_HOMECATEGORY_PROFILE_SCREEN_SET = 'ScreenHome-ProfileUpdate';

var GIGYA_LOGIN_LINK_CONTAINER = 'gigya-login'; 
var GIGYA_LOGIN_MOBILE = 'mobile-login-button';
var GIGYA_LOGOUT_LINK_CONTAINER = 'gigya_controls_logout';
var GIGYA_LOGOUT_MOBILE = 'gigya_controls_logout_mobile';
var GIGYA_WELCOME_TEXT = 'Bienvenido';
var GIGYA_REGISTER_TEXT = 'Iniciar Sesión';
var GIGYA_LOGOUT_TEXT = 'Salir';

var GIGYA_IS_LOGGED = false;
var GIGYA_PROFILE = null;
var GIGYA_PROFILE_URL = "";
var categoria = "";
var dinamica = "";
var loginObject;
var numeroCelular;
var customLangParams = {};

var screenSetToShow =  GIGYA_OTHERCATEGORY_LOGIN_SCREEN_SET;
var screenToShow = GIGYA_OTHERCATEGORY_SCREEN;
var screenSetProfile = GIGYA_OTHERCATEGORY_PROFILE_SCREEN_SET;
var screenToShowNews = GIGYA_NEWS_OTHERCATEGORY_SCREEN;
var registroSocial = false;
var uidSinEmal = "";
var contenido = "";
var conDinamica = false;
      
jQuery(document).ready(function ($) {
    getScreens();
    getPost();
    loadGigya();
}
);

var getScreens = function() {
    screenSetToShow =  GIGYA_OTHERCATEGORY_LOGIN_SCREEN_SET;
    screenToShow = GIGYA_OTHERCATEGORY_SCREEN;
    screenSetProfile = GIGYA_OTHERCATEGORY_PROFILE_SCREEN_SET;
    screenToShowNews = GIGYA_NEWS_OTHERCATEGORY_SCREEN;
    
    var metas = document.getElementsByTagName("meta");
    for (var i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == "article:category")
        {
            categoria = metas[i].getAttribute("content");
            console.log('meta');
            console.log(metas[i]);
            console.log('categoria: ' + categoria);
        }
    }
    
    if (categoria == "dinamicas") {
        categoria = "Dinamicas";
        console.log('Cambiando categoria a: ' + categoria);
    } else if (categoria == "tecnologia") {
        categoria = "Tecnologia";
        console.log('Cambiando categoria a: ' + categoria);
    } else if (categoria == "oferta-comercial") {
        categoria = "OfertaComercial";
        console.log('Cambiando categoria a: ' + categoria);
    } else if (categoria == "responsabilidad-social") {
        categoria = "ResponsabilidadSocial";
        console.log('Cambiando categoria a: ' + categoria);
    } else if (categoria == "deportes") {        
        categoria = "Deportes";
        console.log('Cambiando categoria a: ' + categoria);
    }  else if (categoria == "entretenimiento") {        
        categoria = "Entretenimiento";
        console.log('Cambiando categoria a: ' + categoria);
    } else {
      categoria = "Default";
      screenSetToShow =  GIGYA_HOMECATEGORY_LOGIN_SCREEN_SET;
      screenToShow = GIGYA_HOMECATEGORY_SCREEN;
      screenSetProfile = GIGYA_HOMECATEGORY_PROFILE_SCREEN_SET;  
      screenToShowNews = GIGYA_NEWS_HOMECATEGORY_SCREEN;
    };
};

var getContentType = function() {
    var metas = document.getElementsByTagName("meta");
    for (var i = 0; i < metas.length; i++) {
        if (metas[i].getAttribute("property") == "og:type")
        {
            contenido = metas[i].getAttribute("content");
            console.log('meta');
            console.log(metas[i]);
            console.log('contenido: ' + contenido);
        }
    }
}

var getPost = function() {
    var cadenaURL = document.URL;
    if (cadenaURL.endsWith("/")) {
        cadenaURL=cadenaURL.substr(0,(cadenaURL.length - 1));
    } else if (cadenaURL.endsWith("/#")) {
        cadenaURL=cadenaURL.substr(0,(cadenaURL.length - 2));
    }        
    var posicion = cadenaURL.lastIndexOf('/');
    if (posicion > 0) {
        dinamica = cadenaURL.substring(posicion + 1);
    }
    console.log('Nombre de post: ' + dinamica);
}

var loadGigya = function () {
    console.log('loadGigya');
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = "http://cdn.gigya.com/js/gigya.js?apiKey=" + GIGYA_API;
    document.getElementsByTagName('head')[0].appendChild(s);
};

var handleConnectionAdded = function (loginEvent) {
    console.log('handleConnectionAdded');
};

var onGigyaServiceReady = function (serviceName) {
    gigya.accounts.addEventHandlers({
        onLogin: handleLogin,
        onLogout: evalSession,
        onConnectionAdded: handleConnectionAdded
    }
    );
    evalSession();
};

function populateForm(eventObj) {
    console.log('populateForm');
    console.log(eventObj);
    console.log(eventObj.profile);
    if (categoria === 'Dinamicas') {
        getPost();
        console.log('categoria en populateForm: ' + dinamica);
        jQuery("input[name='data.dinamica']").val(dinamica);
        console.log('Dinamica asignada: ' + dinamica);
    };
};

function errorHandler(eventObj) {
    console.log("failed with error: \n" + eventObj.errorMessage + "\n" + eventObj.errorDetails);
    console.log(eventObj);
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var enviarDatos = function (eventObj) {
    console.log('enviarDatos:');
    if ((eventObj.form === 'gigya-login-form') || (eventObj.form === 'div.gigya-social-login')) {
        return;
    }
    if ( (eventObj.screen === 'gigya-complete-registration-screen') && (eventObj.response.operation === '/accounts.getAccountInfo')) {
        return;
    }
    if (eventObj.screen === 'gigya-complete-registration-screen') {
        registroSocial = true;
    } else {
        registroSocial = false;
    }
    var path = "http://holatelcel.com/wp-content/plugins/gex-mailchimp-salesforce/";
    var facebookId;
    var twitterId;
    var facebook;
    var twitter;
    var facebookUrl;
    var twitterUrl;
    var res = eventObj; //HT.loginData;
    var edad;
    var fechaNacimiento;
    console.log('res:');
    console.log(res);

    if (loginObject) {
        if (loginObject.provider != '') {
            if (loginObject.provider === 'facebook') {
                facebookUrl = loginObject.profile.profileURL;
            } else if (loginObject.provider === 'twitter') {
                twitterUrl = loginObject.profile.profileURL;
            }
        }
    }

    fechaNacimiento = res.data.cumpleanios2;
    edad = getAge(fechaNacimiento);
    var data = {
        d: getParameterByName('d'),
        email: encodeURIComponent(res.profile.email),
        name: res.profile.firstName,
        lastName: res.profile.lastName,
        secondLastName: '',
        age: edad,
        phone: numeroCelular,
        type: res.data.tipoServicio,
        company: encodeURIComponent(res.data.companiaCelular),
        state: res.profile.state,
        facebookUrl: facebookUrl,
        facebookId: facebookId,
        facebook: facebook,
        twitterUrl: twitterUrl,
        twitterId: twitterId,
        twitter: twitter,
        gigyaId: res.response.UID,
    };
    
    if (typeof(data.gigyaId) === 'undefined') {
        data.gigyaId = uidSinEmal;
    };
    
    if (typeof(data.gigyaId) === 'undefined') {
        console.log('enviarDatos canceldo por UID undefined');
        return;
    };
    console.log('enviarDatos::data');
    console.log(data);
    var nombre_dinamica = dinamica; //"wqtyyplchk2"; //
    console.log('enviarDatos::nombre_dinamica');
    console.log(nombre_dinamica);
    try {
        jQuery.ajax({
            type: 'POST',
            url: path + 'newsletter_signup.php',
            data: 'um=' + data.email + "&name=" + data.name + "&state=" + data.state + "&age=" + data.age + "&ph=" + data.phone + "&type=" + data.type + "&cy=" + data.company + "&uid=" + data.gigyaId + '&facebookId=' + data.facebookId + '&facebookUsername=' + data.facebook + '&twitterId=' + data.twitterId + '&twitterUsername=' + data.twitter + '&lastName=' + data.lastName + '&secondLastName=' + data.secondLastName + '&d=' + data.d + '&type=' + data.type + '&facebookUrl=' + data.facebookUrl + '&twitterUrl=' + data.twitterUrl + '&nombre_dinamica=' + nombre_dinamica,
            success: function (data) {
                console.log('SW Ejecutado - data::');
                console.log(data);
                if (data == "1") {
                    jQuery('#warning-result').css('display', 'none');
                    jQuery('#success-result').css('display', 'block');
                    jQuery('#success-result').html('Tu correo ha quedado registrado en el newsletter.');
                    jQuery('#newsletter-button').css('display', 'none');
                    jQuery('#newsletter-desc').css('display', 'none');
                } else if (data == "2") {
                    jQuery('#warning-result').css('display', 'block');
                    jQuery('#success-result').css('display', 'none');
                    jQuery('#warning-result').html('El correo ya esta registrado en el newsletter.');
                    jQuery('#newsletter-button').css('display', 'none');
                    jQuery('#newsletter-desc').css('display', 'none');                    
                } else {
                    jQuery('#warning-result').css('display', 'block');
                    jQuery('#success-result').css('display', 'none');
                    jQuery('#warning-result').html('Se produjo un error: ' + data);
                    jQuery('#newsletter-button').html('¡Ya estas registrado!');
                }

            }  // on success      
        }); // AJAX call 
    } catch (err) {
        console.log(err);
    }
};

var sendData = function (eventObj) {
    if (categoria == "Dinamicas") {
        console.log('Enviar datos a servicio web');
        console.log('eventObj:');
        console.log(eventObj);
        if (eventObj.response.errorCode === '206001') {
            uidSinEmal = eventObj.response.UID;
        };
            console.log('Llamando a enviarDatos');
            enviarDatos(eventObj);
        
    }
};

var showLoginNormal = function () {
    conDinamica = false;
    console.log('showLoginNormal');
    getScreens();
    var loginParams = {
        screenSet: screenSetToShow,
        startScreen: screenToShow,
        onError: errorHandler,
        onAfterScreenLoad: populateForm,
        onBeforeSubmit: validarDatos,
        customLang: customLangParams
    };
    console.log('loginParams::');
    console.log(loginParams);
    gigya.accounts.showScreenSet(loginParams);
};

var showLogin = function () {
    conDinamica = true;
    console.log('showLogin');
    screenToShow="gigya-login-screen";
    var loginParams = {
        screenSet: screenSetToShow,
        startScreen: screenToShow,
        onError: errorHandler,
        onAfterScreenLoad: populateForm,
        onBeforeSubmit: validarDatos,
        onAfterSubmit: sendData,
        customLang: customLangParams
    };
    gigya.accounts.showScreenSet(loginParams);
};

var showRegistration = function () {
    console.log('showRegistration');
    console.log('GIGYA_IS_LOGGED=' + GIGYA_IS_LOGGED);
    
    var loginParams = {
        screenSet: screenSetToShow,
        startScreen: screenToShow,
        onError: errorHandler,
        onAfterScreenLoad: populateForm,
        onBeforeSubmit: validarDatos,
        customLang: customLangParams
    };
    if (categoria === "Dinamicas"){
        conDinamica = true;
        screenSetToShow =  GIGYA_LOGIN_SCREEN_SET;
        screenToShow = GIGYA_DINAMICA_SCREEN;
        screenSetProfile = GIGYA_PROFILE_SCREEN_SET;
        //screenToShowNews = GIGYA_NEWS_DINAMICA_SCREEN;
        loginParams = {
            screenSet: screenSetToShow,
            startScreen: screenToShow,
            onError: errorHandler,
            onAfterScreenLoad: populateForm,
            onBeforeSubmit: validarDatos,
            onAfterSubmit: sendData,
            customLang: customLangParams
        };
    } else {
        conDinamica = false;
    }
    if (GIGYA_IS_LOGGED) {
        loginParams.screenSet = screenSetProfile;
        loginParams.startScreen = categoria;
    } else {
        loginParams.startScreen = "gigya-register-screen";    
    }
    console.log('loginParams::');
    console.log(loginParams);
    gigya.accounts.showScreenSet(loginParams);
};

var showRegisterNews = function () {
    console.log('showRegisterNews');
    screenSetToShow =  GIGYA_OTHERCATEGORY_LOGIN_SCREEN_SET;
    var loginParams = {
        screenSet: screenSetToShow,
        startScreen: screenToShowNews,
        onError: errorHandler,
        onAfterScreenLoad: populateForm,
        onBeforeSubmit: validarDatos,
        onAfterSubmit: sendData,
        customLang: customLangParams
    };
    console.log('loginParams::');
    console.log(loginParams);    
    gigya.accounts.showScreenSet(loginParams);
};

var logOut = function () {
    gigya.accounts.logout();
};

var showProfile = function () {
    console.log('showProfile');
    var profileParams = {
        screenSet: screenSetProfile,
        startScreen: categoria,
        onBeforeSubmit: validarDatos,
        customLang: customLangParams
    };
    
    if (categoria == "Dinamicas") {
        screenSetProfile =  GIGYA_PROFILE_SCREEN_SET;
        screenToShow = "Dinamicas";
        profileParams = {
        screenSet: screenSetProfile,
        startScreen: screenToShow,
        onError: errorHandler,
        onAfterScreenLoad: populateForm,
        onBeforeSubmit: validarDatos,
        onAfterSubmit: sendData,
        customLang: customLangParams
        };
    }
    console.log('profileParams::');
    console.log(profileParams);
    gigya.accounts.showScreenSet(profileParams);
};

var showProfileNormal = function () {
    console.log('showProfileNormal');
    getScreens();
    var profileParams = {
        screenSet: screenSetProfile,
        startScreen: categoria,
        onBeforeSubmit: validarDatos,
        customLang: customLangParams
    };
    
    if (categoria == "Dinamicas") {
        //screenSetProfile =  GIGYA_PROFILE_SCREEN_SET;
        //screenToShow = "Dinamicas-normal";
        profileParams = {
        screenSet: screenSetProfile,
        startScreen: categoria,
        onError: errorHandler,
        onAfterScreenLoad: populateForm,
        onBeforeSubmit: validarDatos,
        customLang: customLangParams
        };
    }
    console.log('profileParams::');
    console.log(profileParams);
    gigya.accounts.showScreenSet(profileParams);
};

var handleLogin = function (loginEvent) {
    loginObject = loginEvent;
    console.log('handleLogin');
    console.log(loginObject);
    evalSession();
    trackLogin(loginEvent);
    if (loginEvent.eventName === 'login') {
        progressiveProfiling(loginEvent);
        console.log(loginEvent.data['loginDinamicas']);
//        if ((categoria == "Dinamicas") && (typeof(loginEvent.data['loginDinamicas']) !== 'undefined'))  {
//            showProfile();
//        } else 
        if ((categoria == "Dinamicas") && (loginEvent.provider === '') && (loginEvent.newUser === false) && (conDinamica === true) ) {
            showProfile();
        } else if ((categoria == "Dinamicas") && ((loginEvent.provider === 'twitter')||(loginEvent.provider === 'instagram')||(loginEvent.provider === 'facebook')) && (loginEvent.newUser === false) && (conDinamica === true)) {
            showProfile();
        };
    };
};

var progressiveProfiling = function(response) {    
    var userGlobalLogins;
    var fieldToUpdate;
    var params;

    fieldToUpdate = 'login'+categoria;
    console.log('fieldToUpdate='+fieldToUpdate);
    
    if ( ( categoria === 'Dinamicas' ) || ( categoria === 'Entretenimiento' ) ) {
        fieldToUpdate = fieldToUpdate + "2";
    }
    
    if (typeof(response.data[fieldToUpdate]) !== 'undefined') {
        userGlobalLogins = parseInt(response.data[fieldToUpdate]) + 1;
        params = {
            data: {
                [fieldToUpdate]: userGlobalLogins
            }
        };
        console.log('params::');
        console.log(params);
        gigya.accounts.setAccountInfo(params);
            
        var tester = function() {
            var a = userGlobalLogins / 3; 
            if (userGlobalLogins === 2) {
              if (categoria !== 'Dinamicas') {
                  var ppCategoria = 'PP'+categoria;
                  gigya.accounts.showScreenSet({
                      screenSet: screenSetProfile,
                      startScreen: ppCategoria,
                      onBeforeSubmit: validarDatos
                  });
              }
            }
        };
        tester();
    } else {
        userGlobalLogins = 1;
        gigya.accounts.setAccountInfo({
            data: {
                [fieldToUpdate]: 1
            }
        });
    }
    console.log('userGlobalLogins='+userGlobalLogins);    
}

var evalSession = function () {
    var evalResponse = function (response) {
        console.log('evalResponse');
        console.log(response);
        if (response.errorCode == 0) {
            GIGYA_IS_LOGGED = true;
            GIGYA_PROFILE = response;
            buildControls(GIGYA_IS_LOGGED, response);
            //progressiveProfiling(response);
        } else {
            GIGYA_IS_LOGGED = false;
            buildControls(GIGYA_IS_LOGGED, response);
        }
    };
    gigya.accounts.getAccountInfo({
        callback: evalResponse}
    );
};

var prellenarCampos = function (account) {
    console.log('prellenarCampos');
    console.log(account);
    jQuery("input[name='email']").val(account.profile.email);
    jQuery("input[name='profile.firstName']").val(account.profile.firstName);
}

var buildControls = function (logged, response) {
    console.log('buildControls');
    console.log(response);
    var controlsElLogin = document.getElementById(GIGYA_LOGIN_LINK_CONTAINER);
    var controlsMobile = document.getElementById(GIGYA_LOGIN_MOBILE);
    var controlsElLogout = document.getElementById(GIGYA_LOGOUT_LINK_CONTAINER);
    var controlsElLogoutMobile = document.getElementById(GIGYA_LOGOUT_MOBILE);
    if (logged) {
        console.log('logged');
        prellenarCampos(response);
        var imgURL = response.profile.thumbnailURL;
        var img = '';
        if (imgURL) {
            img = '<img src="' + response.profile.thumbnailURL + '"  width="28"/>';
        } else {
            img = '<img src="https://cdns.gigya.com/site/images/bsAPI/Placeholder.gif" width="28"/>';
        }
        controlsElLogin.innerHTML = '' +
                img + ' ' + GIGYA_WELCOME_TEXT + ', ' + response.profile.firstName + 
                ' | <a href="#" onClick="showProfileNormal(); ">Mi Perfil</a>';
                
        if(controlsMobile != null){
            
            controlsMobile.innerHTML = '' +
                img + ' ' + GIGYA_WELCOME_TEXT + ', ' + response.profile.firstName +
                ' | <a href="#" onClick="showProfileNormal(); ">Mi Perfil</a>';
            
        }
           
        controlsElLogout.innerHTML = '<a href="#" onClick="logOut(); ">' + GIGYA_LOGOUT_TEXT + '</a>';
        controlsElLogoutMobile.innerHTML = '<a href="#" onClick="logOut(); ">' + GIGYA_LOGOUT_TEXT + '</a>';
        //var elemento = document.getElementById('newsletter-button');
        //elemento.style.display = 'none';
        jQuery('#menu-td-demo-top-menu li:nth-child(3)').css('display', 'none');
        var loginParams = {
            screenSet: screenSetProfile,
            startScreen: screenToShow,
            onError: errorHandler,
            onAfterScreenLoad: populateForm,
            onBeforeSubmit: validarDatos,
            onAfterSubmit: sendData,
            customLang: customLangParams
        };

    } else {
        console.log('NOT logged')
        controlsElLogin.innerHTML = '<a href="#" onClick="showLoginNormal(); ">' + GIGYA_REGISTER_TEXT + '</a> ';
        controlsElLogout.innerHTML = '';
        controlsElLogoutMobile.innerHTML = '';
        if(controlsMobile != null){
            
            controlsMobile.innerHTML = '<a href="#" onClick="showLoginNormal(); ">' + GIGYA_REGISTER_TEXT + '</a> ';
            
        }
        //var elemento = document.getElementById('newsletter-button');
        //elemento.style.display = 'block';
    }
};

var trackLogin = function (loginObject) {
    var profile = loginObject.profile;    
};

function getAge(birthDate) {
    var fecha = birthDate;
    
    var values=fecha.split("/");
    var dia = values[0];
    var mes = values[1];
    var anio = values[2];
 
    var fecha_hoy = new Date();
    var ahora_anio = fecha_hoy.getFullYear();
    var ahora_mes = fecha_hoy.getMonth()+1;
    var ahora_dia = fecha_hoy.getDate();
 
    var edad = ahora_anio - anio;
    if ( ahora_mes < mes ) {
        edad--;
    }
    if ((mes == ahora_mes) && (ahora_dia < dia)) {
        edad--;
    }
 
    return edad;
}

function isNameValid(name) {
    var nameRe = /^[a-zA-Z\xc1\xc9\xcd\xd3\xda\xe1\xe9\xed\xf3\xfa\xd1\xf1\x20]{3,30}$/;
    return nameRe.test(name);
};

function isStateValid(name) {
    var nameRe = /^[a-zA-Z\xc1\xc9\xcd\xd3\xda\xe1\xe9\xed\xf3\xfa\xd1\xf1\x20]{3,30}$/;
    return nameRe.test(name);
};

function isPhoneValid(p) {
    var phoneRe = /^\d{10}$/;
    var digits = p.replace(/\D/g, "");
    return phoneRe.test(digits);
};

function isCarrierValid(name) {
    var nameRe = /^[a-zA-Z\xc1\xc9\xcd\xd3\xda\xe1\xe9\xed\xf3\xfa\xd1\xf1\x20\x26]{3,30}$/;
    return nameRe.test(name);
};

function isServiceValid(name) {
    var nameRe = /^[a-zA-Z\xc1\xc9\xcd\xd3\xda\xe1\xe9\xed\xf3\xfa\xd1\xf1\x20]{3,30}$/;
    return nameRe.test(name);
};

function isBirthdateValid(name) {
    var nameRe = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
    return nameRe.test(name);
};

function isTermsValid(name) {    
    return (name == true);
};

var validarDatos = function(eventObj) {
    console.log('validarDatos');
    console.log(eventObj);
    
    var resultado = "";
    var objeto = eventObj.formData;
    for (var i in objeto) {
        if (objeto.hasOwnProperty(i)) {
            resultado += 'nombreObjeto' + "." + i + " = " + objeto[i] + " valor: " + objeto[i] + "\n";
            switch (i) {
                case 'profile.firstName':
                    console.log('Validar profile.firstName');
                    if (!isNameValid(objeto[i])){
                        alert('Por favor, proporcione un nombre valido.');
                        return false;
                    };
                    break;
                case 'profile.lastName':
                    console.log('Validar profile.lastName');
                    if (!isNameValid(objeto[i])){
                        alert('Por favor, proporcione un apellido valido.');
                        return false;
                    };
                    break;
                case 'data.lastName2':
                    console.log('Validar data.lastName2');
                    if (!isNameValid(objeto[i])){
                        alert('Por favor, proporcione un apellido valido.');
                        return false;
                    };
                    break;
                case 'profile.state':
                    console.log('Validar profile.state');
                    if (!isStateValid(objeto[i])){
                        alert('Por favor, seleccione un estado.');
                        return false;
                    };
                    break;
                case 'profile.phones.default':
                    console.log('Validar profile.phones.default');
                    numeroCelular = objeto[i];
                    if (!isPhoneValid(objeto[i])){
                        alert('Por favor, proporcione un número de celular valido.');
                        return false;
                    };
                    break;
                case 'data.companiaCelular':
                    console.log('Validar data.companiaCelular');
                    if (!isCarrierValid(objeto[i])){
                        alert('Por favor, seleccione una compañia celular.');
                        return false;
                    };
                    break;
                case 'data.tipoServicio':
                    console.log('Validar data.tipoServicio');
                    if (!isServiceValid(objeto[i])){
                        alert('Por favor, seleccione un tipo de servicio.');
                        return false;
                    };
                    break;
                case 'data.cumpleanios2':
                    console.log('Validar data.cumpleanios2');
                    if (!isBirthdateValid(objeto[i])){
                        alert('Por favor, proporcione una fecha válida.');
                        return false;
                    };
                    break;
                case 'data.avisoPrivacidad2':
                    console.log('Validar data.avisoPrivacidad2');
                    if (!isTermsValid(objeto[i])){
                        alert('Por favor, lea y acepte el aviso de privacidad.');
                        return false;
                    };
                    break;
                case 'data.terminosCondiciones2':
                    console.log('Validar data.terminosCondiciones2');
                    if (!isTermsValid(objeto[i])){
                        alert('Por favor, lea y acepte los términos y condiciones.');
                        return false;
                    };
                    break;
            }
        }
    }
    
    console.log('Resultado: \n' + resultado);

};


var newsletter_button = document.getElementById("newsletter-button");
if(newsletter_button != null)
  newsletter_button.onclick = showRegistration;
//var mobile_login = document.getElementById("mobile-login-button");   
//if(mobile_login != null)
//    mobile_login.onclick = showLogin;
    
var menu_newsletter= document.getElementById("menu-td-demo-top-menu").childNodes;
if(menu_newsletter[4] != null)
  menu_newsletter[4].onclick = showRegisterNews;


// End gigya login



// Scroll URLs update


var shares; 
var index;
var share_pos;


function get_positions(){
    share_pos = document.getElementsByClassName('share-buttons');
}

// to avoid jQuery "not a function" errors

(function($) {
  
        get_positions();
        if(share_pos[0] !== null && share_pos[0] !== undefined ){
           share_pos[0].setAttribute("url", window.location.href);
           share_pos[1].setAttribute("url", window.location.href);
           share_pos[0].setAttribute("title", document.title);
           share_pos[1].setAttribute("title", document.title);
            
        }
        
        $( window ).scroll(function() {
              var rect = 0;
              for (index = 0; index < share_pos.length; index++) {
                  rect = share_pos[index].getBoundingClientRect().top - share_pos[0].getBoundingClientRect().top - 398;
                  //console.log("Element "+index+": "+rect);
                  rect = Math.abs(rect); 
                  if((index % 2) === 0){
                     rect = rect + 500; 
                  }
                  var change_url = '';
                  var change_title = '';
                  if((rect-30) <= window.pageYOffset && (rect+30) >= window.pageYOffset){
                   // console.log("Iguales con element "+index);
                    change_url = share_pos[index].getAttribute("url");
                    change_title = share_pos[index].getAttribute("title");
                    document.getElementsByTagName("title")[0].innerHTML = change_title;
                    window.history.pushState( {} , "", change_url );
                  }
              }  
              //console.log("Posicion: " + window.pageYOffset);
              
        });
        
        
        // Moves IOT banner to the top
        var after = document.getElementsByClassName("td-container td-post-template-default")[0];
        var before = document.getElementsByClassName("td-crumb-container")[0];
        var iot_banner = document.getElementById("nombre-del-slot");
        if(iot_banner !== null && iot_banner !== undefined){
            before.parentNode.insertBefore(iot_banner,before);
        }
        
        
        
        // Moves taboola & newsletter divs out of the list slider
        // The slider hides them
        var slider = document.getElementsByClassName("td-iosSlider")[0];
        var footer = document.getElementsByTagName("footer")[0];
        var taboola = document.getElementById("taboola-below-article");
        var boton_reg_nws = document.getElementById("newsletter-outer");
        if(slider !== null && slider !== undefined){
            console.log("Esta el slider");
            footer.parentNode.insertBefore(taboola,footer);
            taboola.parentNode.insertBefore(boton_reg_nws,taboola);
        }
        
        // Moves back button up
        var ret = document.getElementById("back-iot");
        boton_reg_nws.parentNode.insertBefore(ret,boton_reg_nws);
        
        
})(jQuery);
<?php
/*
    Our portfolio:  http://themeforest.net/user/tagDiv/portfolio
    Thanks for using our theme!
    tagDiv - 2016
*/


/**
 * Load the speed booster framework + theme specific files
 */

// load the deploy mode
require_once('td_deploy_mode.php');

// load the config
require_once('includes/td_config.php');
add_action('td_global_after', array('td_config', 'on_td_global_after_config'), 9); //we run on 9 priority to allow plugins to updage_key our apis while using the default priority of 10


// load the wp booster
require_once('includes/wp_booster/td_wp_booster_functions.php');


require_once('includes/td_css_generator.php');
require_once('includes/shortcodes/td_misc_shortcodes.php');
require_once('includes/widgets/td_page_builder_widgets.php'); // widgets


/*
 * mobile theme css generator
 * in wp-admin the main theme is loaded and the mobile theme functions are not included
 * required in td_panel_data_source
 * @todo - look for a more elegant solution(ex. generate the css on request)
 */
require_once('mobile/includes/td_css_generator_mob.php');


/* ----------------------------------------------------------------------------
 * Woo Commerce
 */

// breadcrumb
add_filter('woocommerce_breadcrumb_defaults', 'td_woocommerce_breadcrumbs');
function td_woocommerce_breadcrumbs() {
	return array(
		'delimiter' => ' <i class="td-icon-right td-bread-sep"></i> ',
		'wrap_before' => '<div class="entry-crumbs" itemprop="breadcrumb">',
		'wrap_after' => '</div>',
		'before' => '',
		'after' => '',
		'home' => _x('Home', 'breadcrumb', 'woocommerce'),
	);
}

// use own pagination
if (!function_exists('woocommerce_pagination')) {
	// pagination
	function woocommerce_pagination() {
		echo td_page_generator::get_pagination();
	}
}

// Override theme default specification for product 3 per row


// Number of product per page 8
add_filter('loop_shop_per_page', create_function('$cols', 'return 4;'));

if (!function_exists('woocommerce_output_related_products')) {
	// Number of related products
	function woocommerce_output_related_products() {
		woocommerce_related_products(array(
			'posts_per_page' => 4,
			'columns' => 4,
			'orderby' => 'rand',
		)); // Display 4 products in rows of 1
	}
}

/* ----------------------------------------------------------------------------
 * bbPress
 */
// change avatar size to 40px
function td_bbp_change_avatar_size($author_avatar, $topic_id, $size) {
	$author_avatar = '';
	if ($size == 14) {
		$size = 40;
	}
	$topic_id = bbp_get_topic_id( $topic_id );
	if ( !empty( $topic_id ) ) {
		if ( !bbp_is_topic_anonymous( $topic_id ) ) {
			$author_avatar = get_avatar( bbp_get_topic_author_id( $topic_id ), $size );
		} else {
			$author_avatar = get_avatar( get_post_meta( $topic_id, '_bbp_anonymous_email', true ), $size );
		}
	}
	return $author_avatar;
}
add_filter('bbp_get_topic_author_avatar', 'td_bbp_change_avatar_size', 20, 3);
add_filter('bbp_get_reply_author_avatar', 'td_bbp_change_avatar_size', 20, 3);
add_filter('bbp_get_current_user_avatar', 'td_bbp_change_avatar_size', 20, 3);



//add_action('shutdown', 'test_td');

function test_td () {
    if (!is_admin()){
        td_api_base::_debug_get_used_on_page_components();
    }

}


/**
 * tdStyleCustomizer.js is required
 */
if (TD_DEBUG_LIVE_THEME_STYLE) {
    add_action('wp_footer', 'td_theme_style_footer');
		// new live theme demos
	    function td_theme_style_footer() {
			    ?>
			    <div id="td-theme-settings" class="td-live-theme-demos td-theme-settings-small">
				    <div class="td-skin-body">
					    <div class="td-skin-wrap">
						    <div class="td-skin-container td-skin-buy"><a target="_blank" href="http://themeforest.net/item/newspaper/5489609?ref=tagdiv">BUY NEWSPAPER NOW!</a></div>
						    <div class="td-skin-container td-skin-header">GET AN AWESOME START!</div>
						    <div class="td-skin-container td-skin-desc">With easy <span>ONE CLICK INSTALL</span> and fully customizable options, our demos are the best start you'll ever get!!</div>
						    <div class="td-skin-container td-skin-content">
							    <div class="td-demos-list">
								    <?php
								    $td_demo_names = array();

								    foreach (td_global::$demo_list as $demo_id => $stack_params) {
									    $td_demo_names[$stack_params['text']] = $demo_id;
									    ?>
									    <div class="td-set-theme-style"><a href="<?php echo td_global::$demo_list[$demo_id]['demo_url'] ?>" class="td-set-theme-style-link td-popup td-popup-<?php echo $td_demo_names[$stack_params['text']] ?>" data-img-url="http://demo.tagdiv.com/demos_popup/newspaper/large/<?php echo $demo_id; ?>.jpg"></a></div>
								    <?php } ?>
								    <div class="clearfix"></div>
							    </div>
						    </div>
						    <div class="td-skin-scroll"><i class="td-icon-read-down"></i></div>
					    </div>
				    </div>
				    <div class="clearfix"></div>
				    <div class="td-set-hide-show"><a href="#" id="td-theme-set-hide"></a></div>
				    <div class="td-screen-demo" data-width-preview="380"></div>
				    <div class="td-screen-demo-extend"></div>
			    </div>
			    <?php
	    }

}


function dinamicas_func_new($atts, $content = null){

  extract(shortcode_atts(array(
            "nombre" => ''
        ), $atts));


  $html = '
<script type="text/javascript">
   var nombre_dinamica = "'.$nombre.'";
</script>

<script type="application/javascript" src="/wp-content/plugins/gex-mailchimp-salesforce/js/newsletter_gex1.js"></script>
<script type="text/javascript" lang="javascript" src="http://cdn.gigya.com/JS/gigya.js?apikey=3_m3XzslNIGFNL7teHyINHssbKDdYgeQ-_sJ951SXpTt3sAHG8HXgi46Eadim1iB9Y">
</script>

<style>
    .td-main-content-wrap{<br />        background-image: url("http://holatelcel.exp.mx/wp-content/uploads/2016/09/fondohola-LANDING_PORTABILIDAD.jpg");<br />    }<br />    .portabilidad-content {<br />        background-color: #ffffff;<br />        height:1300px;<br />    }<br />    #portabilidad-form .error {<br />        color: red;<br />    }<br />    #portabilidad-form{<br />        margin: 10px;<br />    }<br />    #portabilidad-form .form-group{<br />        margin: 15px;<br />    }<br />    #portabilidad-form label{<br />        color: #000000;<br />    }<br />    .portabilidad-title {<br />        text-align: center;<br />        color: #4db2ec;<br />    }<br />    .portabilidad-home {<br />        text-align: center;</p>
<p>    }<br />    .portabilidad-submit {<br />        color: #4db2ec;<br />    }<br />    #username {<br />      display: inline;<br />    }<br /></style>

<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/localization/messages_es.js"></script>
<div class="portabilidad-content">
<h2 class="portabilidad-title">NEWSLETTER</h2>
<h3 style="padding: 10px;">Accede con tu red social</h3>
<div id="gigyaLogin"></div>
<form id="portabilidad-form" style="display: none;">&nbsp;
<h2 id="username"></h2>
<div class="form-group"><label for="name">NOMBRE:</label>
<input id="portabilidad-name" class="form-control" name="name" required="" type="text" placeholder="NOMBRE" /></div>
<div class="form-group"><label for="name">APELLIDO PATERNO:</label>
<input id="portabilidad-last-name" class="form-control" name="last-name" required="" type="text" placeholder="APELLIDO PATERNO" /></div>
<div class="form-group"><label for="name">APELLIDO MATERNO:</label>
<input id="portabilidad-second-last-name" class="form-control" name="second-last-name" type="text" placeholder="APELLIDO MATERNO" /></div>
<div class="form-group"><label for="name">EDAD:</label>
<input id="portabilidad-age" class="form-control" max="120" min="1" name="age" type="number" placeholder="EDAD" /></div>
<div class="form-group"><label for="phone">TELEFONO:</label>
<input id="portabilidad-phone" class="form-control" maxlength="10" name="phone" required="" type="text" placeholder="TELEFONO" /></div>
<div class="form-group"><label for="phone">TIPO DE PLAN:</label>
<select id="type" class="form-control" required="">
<option value="0">Prepago</option>
<option value="1">Pospago</option>
</select></div>
<div class="form-group"><label for="state">ELIGE TU OPERADOR:</label>
<select id="portabilidad-operador" class="form-control" name="company" required="">
<option value="">Selecciona un operador</option>
<option value="Telcel">Telcel</option>
<option value="AT&amp;T">AT&amp;T</option>
<option value="Movistar">Movistar</option>
<option value="Unefon">Unefon</option>
<option value="Virgin">Virgin Mobile</option>
<option value="Otro">Otro</option>
</select></div>
<div class="form-group"><label for="email">E-MAIL:</label>
<input id="portabilidad-email" class="form-control" name="email" required="" type="email" placeholder="E-MAIL" /></div>
<div class="form-group"><label for="state">EN QUE ESTADO VIVES:</label>
<select id="portabilidad-state" class="form-control" name="state" required="">
<option value="">Selecciona un estado</option>
<option value="Ciudad de México">Ciudad de México</option>
<option value="Aguascalientes">Aguascalientes</option>
<option value="Baja California">Baja California</option>
<option value="Baja California Sur">Baja California Sur</option>
<option value="Campeche">Campeche</option>
<option value="Chiapas">Chiapas</option>
<option value="Chihuahua">Chihuahua</option>
<option value="Coahuila">Coahuila</option>
<option value="Colima">Colima</option>
<option value="Durango">Durango</option>
<option value="Estado de México">Estado de México</option>
<option value="Guanajuato">Guanajuato</option>
<option value="Guerrero">Guerrero</option>
<option value="Hidalgo">Hidalgo</option>
<option value="Jalisco">Jalisco</option>
<option value="Michoacán">Michoacán</option>
<option value="Morelos">Morelos</option>
<option value="Nayarit">Nayarit</option>
<option value="Nuevo León">Nuevo León</option>
<option value="Oaxaca">Oaxaca</option>
<option value="Puebla">Puebla</option>
<option value="Querétaro">Querétaro</option>
<option value="Quintana Roo">Quintana Roo</option>
<option value="San Luis Potosí">San Luis Potosí</option>
<option value="Sinaloa">Sinaloa</option>
<option value="Sonora">Sonora</option>
<option value="Tabasco">Tabasco</option>
<option value="Tamaulipas">Tamaulipas</option>
<option value="Tlaxcala">Tlaxcala</option>
<option value="Veracruz">Veracruz</option>
<option value="Yucatán">Yucatán</option>
<option value="Zacatecas">Zacatecas</option>
</select></div>
<div class="form-group"><label for="terms">Acepto los <a href="http://www.telcel.com/terminos-y-condiciones" target="_blank">términos y condiciones</a></label>
<input id="terms" class="form-control" name="email" required="" type="checkbox" /></div>
<div class="form-group"><label for="priv">Acepto el <a href="http://www.telcel.com/aviso-de-privacidad" target="_blank">aviso de privacidad</a></label>
<input id="priv" class="form-control" name="email" required="" type="checkbox" /></div>
<div class="form-group" style="text-align: center;"><input class="btn btn-default portabilidad-submit" type="submit" value="ENVIAR" /></div>
<div id="warning-result" class="alert alert-danger" style="display: none;"></div>
<div id="success-result" class="alert alert-success" style="display: none;"></div>
<div style="text-align: center; margin-top: 100px; color: #4db2ec;"><a class="portabilidad-home" href="/">IR A LA PÁGINA DE INICIO</a></div>
</form></div>
  
  ';
  
  return $html;


}

add_shortcode("crea_dinamica", "dinamicas_func_new");

function SearchFilter($query) {

  if ($query->is_search) {

    $query->set('post_type', 'post');

  }

  return $query;

}

 
add_filter('pre_get_posts','SearchFilter');



function flowics_func($atts, $content = null){

  extract(shortcode_atts(array(
            "exp" => '',
            "vis"=>''
        ), $atts));


  $html = '
      <div class="flowics-viz" data-fluid="yes" data-ssl="yes" data-experience="'.$exp.'" data-visualization="'.$vis.'"></div>
      <script src="https://viz.flowics.com/public/scripts/embed"></script> 
      ';
  
  return $html;


}

add_shortcode("flowics", "flowics_func");



<?php
/**
 * The single post loop Default template
 **/

if (have_posts()) {
    the_post();

    $td_mod_single = new td_module_single($post);
  
    $register_msg = array();
    $register_msg['tecnologia'] = array('title'=>'¿Eres amante de la tecnología?', 
                                    'text'=>'Descubre las nuevas tendencias y lo mas relevante en innovación.');
    $register_msg['entretenimiento'] = array('title'=>'¿Te gusta el cine, la música y las series?', 
                                    'text'=>'Mantente siempre actualizado con las últimas noticias del mundo del entretenimiento.');
    $register_msg['deportes'] = array('title'=>'¿Eres amante de los deportes?', 
                                    'text'=>'Entérate de lo último y lo más relevante de tu deporte favorito.');
    $register_msg['oferta-comercial'] = array('title'=>'Oferta comercial:', 
                                    'text'=>'Conoce los mejores planes y promociones que Telcel, la mejor red con la mayor cobertura tiene para ti.');
    $register_msg['responsabilidad-social-2'] = array('title'=>'La naturaleza nos llama:', 
                                    'text'=>'Descubre como juntos podemos ayudar a vivir en un mundo mejor.');
    $register_msg['responsabilidad-social'] = array('title'=>'La naturaleza nos llama:', 
                                    'text'=>'Descubre como juntos podemos ayudar a vivir en un mundo mejor.');
    $register_msg['dinamicas'] = array('title'=>'¡Gana premios, boletos y sorpresas!', 
                                       'text'=>'Para participar:');
    $register_msg['generico'] = array('title'=>' ¡Queremos mejorar tu experiencia!', 
                                    'text'=>'Ayúdanos a conocer lo que te gusta y proporcionarte la información que te interesa.');
  
   
     $category = get_the_category();
     $category_parent_id = $category[0]->category_parent;
     if ( $category_parent_id != 0 ) {
         $category_parent = get_term( $category_parent_id, 'category' );
         $main_cat = $category_parent->slug;
     } else {
         $main_cat = $category[0]->slug;
     }
  
     $id = get_the_id();
  
  
     $primary_cat_id=get_post_meta($id,'_yoast_wpseo_primary_category',true);
  
    
     if($primary_cat_id){
      
       $primary_cat = get_term($primary_cat_id, 'category');
       if(isset($primary_cat->slug)){
         if($primary_cat->slug == 'i-o-t'){
         
            echo '
                 <div id="nombre-del-slot" style="display: none">
                  <a href="http://holatelcel.com/i-o-t">
                   <img src="http://holatelcel.com/wp-content/uploads/2017/05/banner-iot.jpg"/>
                  </a>
                 </div>
                ';
                
         }
       
       
       }
         
     }
  
  
    ?>

   <article id="post-<?php echo $td_mod_single->post->ID;?>" class="<?php echo join(' ', get_post_class());?>" <?php echo $td_mod_single->get_item_scope();?>>
        <div class="td-post-header">

            <?php echo $td_mod_single->get_category(); ?>

            <header class="td-post-title">
                <?php echo $td_mod_single->get_title();?>


                <?php if (!empty($td_mod_single->td_post_theme_settings['td_subtitle'])) { ?>
                    <p class="td-post-sub-title"><?php echo $td_mod_single->td_post_theme_settings['td_subtitle'];?></p>
                <?php } ?>


                <div class="td-module-meta-info">
                    <?php echo $td_mod_single->get_author();?>
                    <?php echo $td_mod_single->get_date(false);?>
                    <?php echo $td_mod_single->get_comments();?>
                    <?php
                        //echo $td_mod_single->get_views();
                    ?>
                </div>

            </header>

        </div>

        <?php
  
         //echo $td_mod_single->get_social_sharing_top();
         
         echo '
         <div id="social-sharing-posts" class="share-buttons">
         <div class="fb-share-button" data-href="'.get_permalink().'"
         data-layout="button"></div>
    <a href="https://twitter.com/share" class="twitter-share-button"
       data-url="'.get_permalink().'">Tweet</a>
       <div><a data-pin-do="buttonBookmark" data-pin-lang="es" data-pin-tall="true" data-pin-save="true" href="https://es.pinterest.com/pin/create/button/"></a></div>
       <div class="g-plus" data-action="share" data-href="'.get_permalink().'" data-annotation="none" ></div>
       </div>
            ';
         
        ?>


        <div class="td-post-content">

        <?php
        // override the default featured image by the templates (single.php and home.php/index.php - blog loop)
        if (!empty(td_global::$load_featured_img_from_template)) {
            echo $td_mod_single->get_image(td_global::$load_featured_img_from_template);
        } else {
            echo $td_mod_single->get_image('td_696x0');
        }
  
  
         $alm_cat = $main_cat;
  
        ?>

        <?php echo $td_mod_single->get_content();
  if($main_cat != 'deportes' && $main_cat != 'tecnologia' && $main_cat != 'dinamicas' && $main_cat != 'responsabilidad-social' && $main_cat != 'entretenimiento' && $main_cat != 'oferta-comercial' && $main_cat != 'responsabilidad-social-2'){
            
               $main_cat = 'generico';    
            }
  
            $texto_custom = '';
            $titulo_custom = '';
            $texto_custom = get_post_meta($id, 'texto_dinamica', true);
            $titulo_custom = get_post_meta($id, 'titulo_dinamica', true);
  
            if($texto_custom != '')
               $register_msg[$main_cat]['text'] = $texto_custom;
            
            if($titulo_custom != '')
               $register_msg[$main_cat]['title'] = $titulo_custom;
          
  
           if(isset($primary_cat->slug)){
               if($primary_cat->slug == 'i-o-t'){
         
                echo '
                 
                   <div id="back-iot">
                       <a href="http://holatelcel.com/i-o-t" style="text-decoration: none;">
                       Regresar
                       </a>
                   </div>
                 
                ';
                
               }
       
       
           } 
  
  
  
  
  
  
  
          echo '
          
          
          
<script>
var registrar_tag = document.getElementsByClassName("td-post-content")[0];
var taboola = document.getElementById("taboola-below-article");
var outer_registrar = document.createElement("div");
var title_registrar = document.createElement("div");
var desc_registrar = document.createElement("div");
var button_registrar = document.createElement("div");

button_registrar.setAttribute("id","newsletter-button");
outer_registrar.setAttribute("id","newsletter-outer");
title_registrar.setAttribute("id","newsletter-title");
desc_registrar.setAttribute("id","newsletter-desc");

button_registrar.innerHTML = "Regístrate aquí";
title_registrar.innerHTML = "'.$register_msg[$main_cat]['title'].'";
desc_registrar.innerHTML = "'.$register_msg[$main_cat]['text'].'";


outer_registrar.appendChild(title_registrar);
outer_registrar.appendChild(desc_registrar);
outer_registrar.appendChild(button_registrar);

if(taboola !== null && taboola !== undefined)
  taboola.parentNode.insertBefore(outer_registrar,taboola);
</script>
';
  
  
  /* echo '
                     <div id="taboola-below-article-thumbnails"></div>
                 <script type="text/javascript">
                    window._taboola = window._taboola || [];
                    _taboola.push({
                       mode: "alternating-thumbnails-a",
                    container: "taboola-below-article-thumbnails",
                             placement: "Below Article Thumbnails",
                             target_type: "mix"
                            });
                      </script>
                     ';
          
          
          */
          
          
          
          ?>
        </div>


        <footer>
            <?php echo $td_mod_single->get_post_pagination();?>
            <?php echo $td_mod_single->get_review();?>

            <div class="td-post-source-tags">
                <?php echo $td_mod_single->get_source_and_via();?>
                <?php echo $td_mod_single->get_the_tags();?>
            </div>

            <?php
  
             //echo $td_mod_single->get_social_sharing_bottom();
             
           echo '
         <div id="social-sharing-posts-b" class="share-buttons">
         <div class="fb-share-button" data-href="'.get_permalink().'"
         data-layout="button"></div>
    <a href="https://twitter.com/share" class="twitter-share-button"
       data-url="'.get_permalink().'">Tweet</a>
       <div><a data-pin-do="buttonBookmark" data-pin-lang="es" data-pin-tall="true" data-pin-save="true" href="https://es.pinterest.com/pin/create/button/"></a></div>
       <div class="g-plus" data-action="share" data-href="'.get_permalink().'" data-annotation="none" ></div>
       </div>
            ';
            
          
            ?>
            <?php echo $td_mod_single->get_next_prev_posts();?>
            <?php echo $td_mod_single->get_author_box();?>
	        <?php echo $td_mod_single->get_item_scope_meta();?>
        </footer>

    </article> <!-- /.post -->

<?php
  //echo $td_mod_single->related_posts();
  
     

  echo do_shortcode('[ajax_load_more max_pages="6" posts_per_page="1" exclude="'.$id.'" category="'.$alm_cat.'" scroll_distance="100"]');
  
  
    ?>

    
    

<?php
} else {
    //no posts
    echo td_page_generator::no_posts();
}
/* Ads DFP */
var adUnit = "/72776159/holatelcel";
var slots = [];
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
    var gads = document.createElement("script");
    gads.async = true;
    gads.type = "text/javascript";
      var useSSL = "https:" == document.location.protocol;
      gads.src = (useSSL ? "https:" : "http:") +
      "//www.googletagservices.com/tag/js/gpt.js";
      var node = document.getElementsByTagName("script")[0];
      node.parentNode.insertBefore(gads, node);
})();
   
googletag.cmd.push(function() {
  
var sizeLead = googletag.sizeMapping().
addSize([1024, 768],[[970, 90],[970,250]]). //Desktop.
addSize([980, 690], [728,90]). // /Tablet.
addSize([0, 0], [300, 100]). // Mobile.
build();
 
var sizeSidebar = googletag.sizeMapping().
addSize([0,0],[300,250]). //All
build();
 

var sizeArticle = googletag.sizeMapping().
addSize([1024, 768],[728,90]). //Desktop.
addSize([980, 690],[728,90]). // /Tablet.
addSize([0,0],[300,250]). // Mobile.
build();


slots['ad-top-sidebar'] = googletag.defineSlot(adUnit, [300, 250],"ad-top-sidebar").defineSizeMapping(sizeSidebar).setCollapseEmptyDiv(true).addService(googletag.pubads()).setTargeting("pos","1").setTargeting("contentId",location.pathname);

slots['ad-inline-sidebar'] = googletag.defineSlot(adUnit, [300, 250],"ad-inline-sidebar").defineSizeMapping(sizeSidebar).setCollapseEmptyDiv(true).addService(googletag.pubads()).setTargeting("pos","2").setTargeting("contentId",location.pathname);

slots['ad-top-leaderboard'] = googletag.defineSlot(adUnit, [[970, 90],[970,250]],"ad-top-leaderboard").defineSizeMapping(sizeLead).setCollapseEmptyDiv(true).addService(googletag.pubads()).setTargeting("pos","1").setTargeting("contentId",location.pathname);

slots['ad-inline-leaderboard'] = googletag.defineSlot(adUnit, [[955, 90],[955,75],[955,31],[955,70],[970,90]],"ad-inline-leaderboard").defineSizeMapping(sizeLead).setCollapseEmptyDiv(true).addService(googletag.pubads()).setTargeting("pos","2").setTargeting("contentId",location.pathname);

slots['ad-bottom-leaderboard'] = googletag.defineSlot(adUnit, [[955, 90],[955,75],[955,31],[955,70],[970,90]],"ad-bottom-leaderboard").defineSizeMapping(sizeLead).setCollapseEmptyDiv(true).addService(googletag.pubads()).setTargeting("pos","3").setTargeting("contentId",location.pathname);

slots['ad-inline-article'] = googletag.defineSlot(adUnit, [300, 250],"ad-inline-article").defineSizeMapping(sizeArticle).setCollapseEmptyDiv(true).addService(googletag.pubads()).setTargeting("pos","2").setTargeting("contentId",location.pathname);

slots['nativeTelcel'] = googletag.defineSlot(adUnit, 'fluid', 'nativeTelcel').addService(googletag.pubads());
  
googletag.pubads().setTargeting("ksg", Krux.segments);
googletag.pubads().setTargeting("kuid", Krux.user);  

googletag.enableServices();
  

  
});
/* Ads DFP End */